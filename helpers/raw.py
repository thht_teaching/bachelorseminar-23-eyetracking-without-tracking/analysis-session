from obob_mne.mixins.raw import LoadFromSinuhe, AdvancedEvents, AutomaticBinaryEvents
from obob_mne.events import read_events_from_analogue
import pandas as pd
import numpy as np
import mne
import re
import scipy.signal
from pathlib import Path
from pymatreader import read_mat
import warnings
from warnings import warn
import eelbrain as eb
from copy import deepcopy


class AutomaticBinaryEventsWithMetadata(AutomaticBinaryEvents):
    def _process_events(self):
        super(AutomaticBinaryEvents, self)._process_events()

        conditions_metadata = {}
        all_raw_metadata = []

        if self.condition_triggers:
            condition_trigger = self._events[0, 2]
            self._events = np.delete(self._events, (0), axis=0)

            for allcond_key, allcond_value in self.condition_triggers.items():
                conditions_metadata[allcond_key] = self._decode_bin_trigger(
                    condition_trigger,
                    allcond_value
                )

        for cur_evt_code in self._events[:, 2]:
            cur_metadata = deepcopy(conditions_metadata)
            for stim_group_name, stim_group_choices in \
                    self.stimulus_triggers.items():
                cur_metadata[stim_group_name] = self._decode_bin_trigger(
                    cur_evt_code, stim_group_choices
                )

            all_raw_metadata.append(cur_metadata)


        self._evt_metadata = pd.DataFrame(all_raw_metadata)



class EelbrainMixin:
    @property
    def eelbrain_time_dim(self):
        return eb.UTS(
            tmin=self.times.min(),
            tstep=1. / self.info['sfreq'],
            nsamples=self.times.shape[0]
        )

    @property
    def eelbrain_meg_sensor_dim(self):
        meg_channel_names = [x['ch_name'] for x in self.info['chs'] if
                             x['ch_name'].startswith('MEG')]
        meg_channel_locations = [x['loc'][:3] for x in self.info['chs']
                                 if x['ch_name'].startswith('MEG')]

        return eb.Sensor(meg_channel_locations, meg_channel_names)

    @property
    def eelbrain_meg_data(self):
        meg_data = self.copy().load_data().pick('meg').get_data()
        return eb.NDVar(
            meg_data,
            (self.eelbrain_meg_sensor_dim, self.eelbrain_time_dim),
            name='brain'
        )

    def get_eelbrain_from_channels(self, channels):
        data = self.get_data(channels)
        return eb.NDVar(
            data,
            (eb.Categorial('/'.join(channels), channels), self.eelbrain_time_dim)
        )


class EyeTrackerMixin:
    def __init__(self, *args, eye_tracking_mat_fname=None, **kwargs):
        self._this_eye_tracking_mat_fname = eye_tracking_mat_fname

        super().__init__(*args, **kwargs)


    def _add_eye_tracking_data(self):
        x_data, y_data, trigger_data = self._load_eyetracking_data()
        x_data_aligned, x_shifted_by = self._align_eye_to_meg(x_data, trigger_data)
        y_data_aligned, y_shifted_by = self._align_eye_to_meg(y_data, trigger_data)

        eye_data = np.vstack((x_data_aligned, y_data_aligned))
        eye_info = mne.create_info(
            ['eyetracker_x', 'eyetracker_y'],
            sfreq=self.info['sfreq'],
            ch_types='bio'
        )

        eye_data = np.resize(eye_data, (2, self.n_times))

        eye_raw = mne.io.RawArray(
            data=eye_data,
            info=eye_info
        )

        self.add_channels([eye_raw], force_update_info=True)

    @property
    def _eye_tracking_mat_fname(self):
        return NotImplementedError
    
    @property
    def eye_tracking_mat_fname(self):
        if self._this_eye_tracking_mat_fname is not None:
            return self._this_eye_tracking_mat_fname
        
        return self._eye_tracking_mat_fname

    def _load_eyetracking_data(self):
        eye_raw_data = read_mat(self.eye_tracking_mat_fname)['data']
        x_data = np.nanmean(eye_raw_data[:, [1, 4]].T, axis=0)
        y_data = np.nanmean(eye_raw_data[:, [2, 5]].T, axis=0)
        trigger_data = eye_raw_data[:, 10].T

        tmp = pd.Series(x_data)
        tmp.interpolate(limit_direction='both',
                        limit_area='inside',
                        inplace=True)
        x_data = tmp.values

        tmp = pd.Series(y_data)
        tmp.interpolate(limit_direction='both',
                        limit_area='inside',
                        inplace=True)
        y_data = tmp.values

        assert y_data.shape == x_data.shape

        x_data = scipy.signal.decimate(x_data, 2, ftype='fir')
        y_data = scipy.signal.decimate(y_data, 2, ftype='fir')
        trigger_data = trigger_data[::2]

        return x_data, y_data, trigger_data

    def _align_eye_to_meg(self, eye_data, trigger_data):
        meg_start_trigger = self.events[0, 2]
        meg_start_sample = self.events[0, 0] - self.first_samp
        eye_start_sample = np.min(np.argwhere(trigger_data==meg_start_trigger))
        shift_samples = meg_start_sample - eye_start_sample

        if shift_samples < 0:
            new_data = eye_data[-shift_samples:]
            new_data = new_data[:self.n_times]
            new_data = np.pad(new_data, (0, self.n_times - len(new_data)), 'constant')
        else:
            new_data = np.concatenate((np.zeros((shift_samples, )), eye_data))
            new_data = new_data[0:self.n_times]

        return new_data, shift_samples


class CalibrationRaw(EyeTrackerMixin, LoadFromSinuhe, AdvancedEvents, EelbrainMixin):
    study_acronym = 'th_donottrackmycocktaileye'
    file_glob_patterns = [
        '%s_calibration.fif'
    ]

    sinuhe_root = 'data/fiff/data_raw'

    def __init__(self, subject_id, **kwargs):
        self._subject_id = subject_id

        super().__init__(subject_id=subject_id,
                         **kwargs)

        self.load_data()
        self._add_eye_tracking_data()

    @property
    def _eye_tracking_mat_fname(self):
        eye_data_path = Path('data_in_git', 'eye_tracking', self._subject_id)
        return Path(eye_data_path, 'calibration.mat')

    def _load_events(self):
        if 'calibration_x' not in self.ch_names:
            self._event_id = dict()
            self._events = read_events_from_analogue(self, tolerance=1)

            self.first_event_seconds = (self.events[0, 0] - self.first_samp) / self.info['sfreq']
            self.last_event_seconds = (self.events[-1, 0] - self.first_samp) / self.info['sfreq']

            self._process_events()

    def _process_events(self):
        cal_df = pd.read_parquet('data_in_git/calibration.parquet')
        self.load_data()
        distance_triggers = int(np.median(np.diff(self.events[:, 0]))) + 10
        calibration_xy = np.zeros((2, self._data.shape[1]))

        n_used_evts = 0
        last_sample = -10000
        for cur_evt in self.events:
            samp = cur_evt[0] - self.first_samp
            if samp - last_sample < 1000:
                warn('Found a bogus trigger')
                continue

            last_sample = samp
            if cur_evt[2] not in cal_df['triggers'].values:
                warn(f'Trigger {cur_evt[2]} not found in calibration df. Skipping')
                continue
            coords = cal_df.query(f'triggers=={cur_evt[2]}')[['X', 'Y']].to_numpy()
            calibration_xy[:, samp:samp+distance_triggers] = coords.T
            n_used_evts += 1

        assert n_used_evts == 125

        cal_info = mne.create_info(
            ['calibration_x', 'calibration_y'],
            sfreq=self.info['sfreq'],
            ch_types='stim'
        )

        cal_raw = mne.io.RawArray(
            data=calibration_xy,
            info=cal_info
        )

        self.add_channels([cal_raw], force_update_info=True)

class CocktailSpeechRaw(EyeTrackerMixin, LoadFromSinuhe, AdvancedEvents, EelbrainMixin):
    study_acronym = 'th_donottrackmycocktaileye'
    file_glob_patterns = [
        '%s_cocktail_blk%d.fif'
    ]
    sinuhe_root = 'data/fiff/data_raw'

    sound_files_dict = {
        32: '1_1_m',
        33: '1_1_f',
        34: '1_2_m',
        35: '1_2_f',
        36: '2_1_m',
        37: '2_1_f',
        38: '2_2_m',
        39: '2_2_f',
        40: '3_1_m',
        41: '3_1_f',
        42: '3_2_m',
        43: '3_2_f',
        44: '4_1_m',
        45: '4_1_f',
        46: '4_2_m',
        47: '4_2_f',
    }

    def __init__(self, subject_id, block_nr, **kwargs):
        self._subject_id = subject_id
        self._block_nr = block_nr
        super().__init__(subject_id=subject_id,
                         block_nr=block_nr,
                         **kwargs)

        self.load_data()

        all_wav_files = {x for x in self.evt_metadata[
            ['target_speaker_filename',
             'distractor_filename']].values.flatten() if x is not None}
        all_env_files = [re.sub(r'.wav', '.mat', x) for x in all_wav_files]
        all_env_files = [list(Path('data_in_git', 'envelopes').rglob(f'**/{x}'))[0] for x in all_env_files]
        all_envs = {}

        env_fsample = 0
        for cur_env_file in all_env_files:
            cur_mat = read_mat(cur_env_file)
            cur_env = cur_mat['env_data']['trial']
            env_fsample = cur_mat['env_data']['fsample']
            cur_key = f'{cur_env_file.stem}.wav'

            all_envs[cur_key] = cur_env

        env_channel = np.zeros((2, self.n_times))
        for evt, metadata in zip(self.events, self.evt_metadata.iloc()):
            if metadata['is_target_speaker']:
                cur_fname = metadata['target_speaker_filename']
                ch_idx = 0
            else:
                cur_fname = metadata['distractor_filename']
                ch_idx = 1

            cur_env = all_envs[cur_fname]
            first_idx = evt[0] - self.first_samp
            last_idx = evt[0] + cur_env.shape[0] - self.first_samp

            max_env_size = env_channel.shape[1]-first_idx
            if max_env_size < cur_env.shape[0]:
                warnings.warn(f'Cannot fit env completely into data. I have {cur_env.shape[0] - max_env_size} extra samples!')
                cur_env = np.resize(cur_env, max_env_size)

            env_channel[ch_idx, first_idx:last_idx] = cur_env

        env_info = mne.create_info(['envelope_target', 'envelope_distractor'],
                                   ch_types='bio',
                                   sfreq=env_fsample)
        env_raw = mne.io.RawArray(
            data=env_channel,
            info=env_info,
        )

        self.add_channels([env_raw], force_update_info=True)
        self._add_eye_tracking_data()

    @property
    def _eye_tracking_mat_fname(self):
        eye_data_path = Path('data_in_git', 'eye_tracking', self._subject_id)
        return Path(eye_data_path, f'cocktail_block_{self._block_nr}.mat')

    def _load_events(self):
        self._event_id = dict()
        self._events = read_events_from_analogue(self, tolerance=3)

        self._process_events()

    def _process_events(self):
        ## Load subject mat file in order to check triggers
        subject_mat_data = read_mat(Path('data_in_git', 'subject_data', f'{self._subject_id}.mat'))['subj_data']
        this_start_triggers = subject_mat_data['stims']['cocktail_party']['trigger']['target_book'][:, self._block_nr-1]
        target_speaker_gender_idx = subject_mat_data['cocktail_party']['order']['sex'][:, self._block_nr-1]
        n_speakers = subject_mat_data['cocktail_party']['order']['n_speaker'][:, self._block_nr-1]
        this_start_triggers += target_speaker_gender_idx

        start_events = []
        for idx_start in range(0, 2):
            cur_trigger_from_mat_file = this_start_triggers[idx_start]
            bitwise_and_with_evts = np.bitwise_and(self.events[:, 2], cur_trigger_from_mat_file)
            idx_max = np.where(bitwise_and_with_evts==cur_trigger_from_mat_file)[0]
            if idx_max.size == 0:
                raise RuntimeError('Cannot find trigger')
            if idx_start == 0:
                evt_idx = idx_max[0]
            elif idx_start == 1:
                evt_idx = idx_max[-1]
            else:
                raise RuntimeError('idx_start is wrong!')

            this_evt = self.events[evt_idx, :]
            this_evt[2] = cur_trigger_from_mat_file
            start_events.append(this_evt)

            if n_speakers[idx_start] == 2:
                bitwise_and_with_evts = np.bitwise_and(self.events[:, 2], 64)
                idx_max = np.where(bitwise_and_with_evts==64)[0]
                if idx_max.size != 1:
                    raise RuntimeError('Too many triggers found')

                this_evt = self.events[idx_max[0], :]
                gap_between_triggers_samples = this_evt[0] - start_events[-1][0]
                #if gap_between_triggers_samples < (19*1000) or gap_between_triggers_samples > (21*1000):
                #    raise RuntimeError('Gap between triggers is bad')

                this_evt[2] = 64
                start_events.append(this_evt)

        start_events = np.array(start_events)
        stop_events = mne.pick_events(self.events, include=[2])

        self._events = start_events
        metadata_list = []

        for evt in self._events:
            cur_trigger = evt[2]
            cur_sample = evt[0]
            if cur_trigger < 32 or cur_trigger > 65:
                raise ValueError(f'Unexpected trigger value: {cur_trigger}')
            if 32 <= cur_trigger <= 47:  # Target onset trigger
                stop_evt_idx = np.where(stop_events[:, 0] - cur_sample > 0, stop_events[:, 0], np.inf).argmin()
                stop_evt = stop_events[stop_evt_idx, :]
                assert stop_evt[2] == 2
                target_speaker_gender = 'male'
                if cur_trigger % 2 == 1:
                    target_speaker_gender = 'female'
                file_name = f'{self.sound_files_dict[cur_trigger]}.wav'
                has_distractor = False
                distractor_filename = None
                distractor_onset_sample = np.NAN
                stop_sample = stop_evt[0]

                metadata_list.append({
                    'is_target_speaker': True,
                    'target_speaker_gender': target_speaker_gender,
                    'target_speaker_filename': file_name,
                    'has_distractor': has_distractor,
                    'distractor_filename': distractor_filename,
                    'distractor_speaker_gender': None,
                    'stop_sample': stop_sample,
                    'stop_rel_time': (stop_sample - cur_sample) / self.info['sfreq'],
                    'distractor_onset_sample': distractor_onset_sample
                })
            elif cur_trigger in [64, 65]:  # Distractor Onset
                target_item = metadata_list[-1]
                target_item['has_distractor'] = True
                if target_item['target_speaker_gender'] == 'male':
                    target_item['distractor_speaker_gender'] = 'female'
                else:
                    target_item['distractor_speaker_gender'] = 'male'
                target_item['distractor_onset_sample'] = cur_sample
                target_item['distractor_filename'] = re.sub(r'_[0-9]_', '_3_', target_item['target_speaker_filename'])

                this_item = target_item.copy()
                this_item['is_target_speaker'] = False
                this_item['stop_rel_time'] = (stop_sample - cur_sample) / self.info['sfreq']

                metadata_list.append(this_item)

        self._evt_metadata = pd.DataFrame(metadata_list)

class RawSounds(AutomaticBinaryEventsWithMetadata):
    condition_triggers = {
        'condition_detuned': 1,
        'markov': {
            'random': 2,
            'ordered': 4
        }
    }

    stimulus_triggers = {
        'freq': {
            1: 16,
            2: 32,
            3: 64,
            4: 128
        },
        'tone_detuned': 1
    }
