import mne
from .raw import CocktailSpeechRaw
import eelbrain as eb
from pathlib import Path
import joblib
import numpy as np

maxwell_kwargs = dict()

def get_speech_epochs(subject_id, block_nr,
                      maxfilter=True,
                      trans_maxfilter='default',
                      filter_settings=None,
                      filter_njobs=-1,
                      resample=None,
                      ):
    if filter_settings is None:
        filter_settings = {
            'l_freq': 0.1,
            'h_freq': 30,
        }

    raw = CocktailSpeechRaw(subject_id=subject_id,
                            block_nr=block_nr,
                            preload=True)
    this_metadata = raw.evt_metadata.query('is_target_speaker==True')
    this_events = raw.events[this_metadata.index, :]
    for evt, metadata in zip(this_events, this_metadata.iloc()):
        if metadata['has_distractor']:
            evt[0] = metadata['distractor_onset_sample']

    if maxfilter:
        (ch_noise, ch_flat) = mne.preprocessing.find_bad_channels_maxwell(
            raw, **maxwell_kwargs
        )
        raw.info['bads'] = ch_noise + ch_flat

        if trans_maxfilter == 'default':
            destination = (0, 0, 0.04)
        elif trans_maxfilter == 'no':
            destination = None
        else:
            raise RuntimeError('trans value not supported')

        raw = mne.preprocessing.maxwell_filter(
            raw,
            destination=destination,
            **maxwell_kwargs
        )

    raw.pick_types(meg=True, eog=True, ecg=True, misc=True, stim=True, bio=True)

    if filter_settings is not False:
        filter_settings['n_jobs'] = filter_njobs
        raw.filter(**filter_settings)

    speech_epochs = []

    for trial_nr in range(2):
        cur_metadata = this_metadata.iloc[trial_nr:trial_nr + 1]
        cur_events = this_events[trial_nr:trial_nr + 1, :]
        this_epoch = SpeechEpoch(
            subject_id=subject_id,
            raw=raw,
            events=cur_events,
            metadata=cur_metadata,
            tmin=-1,
            tmax=cur_metadata['stop_rel_time'].values[0],
        )

        if resample is not None:
            this_epoch.load_data().resample(resample)

        speech_epochs.append(this_epoch)

    return speech_epochs


class SpeechEpoch(mne.Epochs):
    eyedecode_data_path = Path('data_in_git/calibration_results')

    def __init__(self, subject_id, *args, **kwargs):
        self.subject_id = subject_id
        super().__init__(*args, **kwargs)


    def add_eye_estimates(self):
        eye_decoding_data = joblib.load(Path(self.eyedecode_data_path, self.subject_id, f'{self.subject_id}.dat'))
        predicted_meg = eb.convolve(eye_decoding_data['meg'].h_scaled, self.eelbrain_meg_data).get_data()
        predicted_eog = eb.convolve(eye_decoding_data['eog'].h_scaled, self.get_eelbrain_from_channels(['EOG001', 'EOG002'])).get_data()

        all_components = eye_decoding_data['ica_object'].get_sources(self)
        eog_components = all_components.get_eelbrain_from_channels([f'ICA{idx:03d}' for idx in eye_decoding_data['ica_eog_comps'][0]])
        predicted_ica = eb.convolve(eye_decoding_data['ica'].h_scaled, eog_components).get_data()

        info = mne.create_info(
            ['meg_est_x', 'meg_est_y',
             'eog_est_x', 'eog_est_y',
             'ica_est_x', 'ica_est_y'],
            sfreq=self.info['sfreq'],
            ch_types='stim'
        )

        data = np.vstack((predicted_meg, predicted_eog, predicted_ica))

        estimates_raw = mne.EpochsArray(
            data=data[np.newaxis, :],
            info=info
        )

        self.add_channels([estimates_raw], force_update_info=True)

    @property
    def eelbrain_time_dim(self):
        return eb.UTS(
            tmin=self.times.min(),
            tstep=1./self.info['sfreq'],
            nsamples=self.times.shape[0]
        )

    @property
    def eelbrain_meg_sensor_dim(self):
        meg_channel_names = [x['ch_name'] for x in self.info['chs'] if
                             x['ch_name'].startswith('MEG')]
        meg_channel_locations = [x['loc'][:3] for x in self.info['chs']
                                 if x['ch_name'].startswith('MEG')]

        return eb.Sensor(meg_channel_locations, meg_channel_names)

    @property
    def eelbrain_meg_data(self):
        meg_data = self.copy().load_data().pick('meg').get_data()
        return eb.NDVar(
            meg_data[0, :, :],
            (self.eelbrain_meg_sensor_dim, self.eelbrain_time_dim),
            name='brain'
        )

    def get_eelbrain_from_channels(self, channels):
        data = self.get_data(channels)
        return eb.NDVar(
            data[0, :, :],
            (eb.Categorial('/'.join(channels), channels), self.eelbrain_time_dim)
        )

    @property
    def eelbrain_target_envelope_data(self):
        envelope_tmp_data = self.get_data('envelope_target')
        return eb.NDVar(envelope_tmp_data[0, 0, :],
                              (self.eelbrain_time_dim, ),
                              name='target_envelope')

    @property
    def eelbrain_distractor_envelope_data(self):
        envelope_tmp_data = self.get_data('envelope_distractor')
        return eb.NDVar(envelope_tmp_data[0, 0, :],
                              (self.eelbrain_time_dim,),
                              name='distractor_envelope')

    @property
    def eelbrain_eyetracker_data(self):
        eye_data = self.get_data(['eyetracker_x', 'eyetracker_y'])
        return eb.NDVar(
            eye_data[0, :, :],
            (eb.Categorial('eyes', ['eyetracker_x', 'eyetracker_y']), self.eelbrain_time_dim)
        )
